import com.github.cliftonlabs.json_simple.JsonObject;
import controller.Dispatcher;
import model.*;
import org.junit.Test;
import tools.JsonParser;
import tools.JsonWriter;

import java.text.ParseException;
import java.util.Map;

public class TestExport {
    private final String INPUT_FILE = "src/main/java/Level2/data/input.json";
    private final String OUTPUT_FILE = "src/main/java/Level2/data/output.json";

    @Test
    public void testExportLevel2() throws ParseException {
        JsonObject jsonData = JsonParser.parse(INPUT_FILE);
        Map<Integer, Company> companies = JsonParser.getCompanies(jsonData);
        Map<Integer, User> users = JsonParser.getUsers(jsonData);

        Dispatcher dispatcher = new Dispatcher();

        dispatcher.distribute(companies.get(1), users.get(1), new GiftCard(50, "2020-09-16"));
        dispatcher.distribute(companies.get(1), users.get(2), new GiftCard(100, "2020-09-16"));
        dispatcher.distribute(companies.get(2), users.get(3), new GiftCard(1000, "2020-09-16"));
        dispatcher.distribute(companies.get(1), users.get(1), new MealVoucher(250, "2020-09-16"));

        DataSet dataSet = new DataSet(companies.values(), users.values(), dispatcher.getDistributions());
        // Just for the exercise purpose
        JsonWriter.write(dataSet, OUTPUT_FILE);
    }
}

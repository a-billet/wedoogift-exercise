package tools;

import model.DataSet;
import com.github.cliftonlabs.json_simple.Jsoner;

import java.io.FileWriter;
import java.io.IOException;

public class JsonWriter {
    public static void write(DataSet dataSet, String fileName) {
        String json = Jsoner.serialize(dataSet);
        json = Jsoner.prettyPrint(json);

        try (FileWriter fileWriter = new FileWriter(fileName)) {
            fileWriter.write(json);
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
}

package application;

import controller.Dispatcher;
import model.Company;
import model.User;
import model.Voucher;

import java.util.HashMap;
import java.util.Map;

public class Controller implements IServices {
    private Dispatcher dispatcher = new Dispatcher();
    private Map<Integer, User> users = new HashMap<>();

    @Override
    public boolean distributeVoucher(Company company, User user, Voucher voucher) {
        users.put(user.getId(), user);
        return dispatcher.distribute(company, user, voucher);
    }

    @Override
    public int getUserBalance(int userId) {
        User user = users.get(userId);
        if (user != null) {
            user.getUserBalance();
        }
        return -1;
    }
}

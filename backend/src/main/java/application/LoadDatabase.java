package application;

import model.Company;
import model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.CommandLineRunner;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

@Configuration
class LoadDatabase {

    private static final Logger log = LoggerFactory.getLogger(LoadDatabase.class);

    @Bean
    CommandLineRunner initClients(UserRepository repository) {

        return args -> {
            log.info("Preloading " + repository.save(new User(1)));
            log.info("Preloading " + repository.save(new User(2)));
        };
    }

    @Bean
    CommandLineRunner initCompanies(CompanyRepository repository) {

        return args -> {
            log.info("Preloading " + repository.save(new Company(1, "Wedoogift")));
            log.info("Preloading " + repository.save(new Company(2, "Wedoofood")));
        };
    }
}

package application;

import model.Company;
import model.User;
import model.Voucher;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;

@ComponentScan
@SpringBootApplication
public class WeDooGiftApplication {
    public static void main(String... args) {
        SpringApplication.run(WeDooGiftApplication.class, args);
    }

    @Bean
    public CommandLineRunner distributeVoucher(Company company, User user, Voucher voucher) {
        return args -> {
        };
    }

    @Bean
    public CommandLineRunner getUserBalance(int userId) {
        return args -> {
        };
    }
}


package application;

import model.Company;
import model.User;
import model.Voucher;

public interface IServices {
    boolean distributeVoucher(Company company, User user, Voucher voucher);

    int getUserBalance(int userId);
}

package application;

import model.Company;
import org.springframework.data.jpa.repository.JpaRepository;

interface CompanyRepository extends JpaRepository<Company, Integer> {

}

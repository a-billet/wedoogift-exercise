package controller;

import model.*;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.atomic.AtomicInteger;

public class Dispatcher {
    private AtomicInteger counter = new AtomicInteger(0);
    private List<Distribution> distributions = new ArrayList<>();

    public boolean distribute(Company company, User user, Voucher voucher) {
        int balance = company.getBalance();
        int amount = voucher.getAmount();
        if (balance < amount) {
            return false;
        }
        int walletId = getWalletId(voucher);
        if (walletId == -1) {
            // Unknown voucher type
            return false;
        }

        user.addVoucher(walletId, voucher);
        Distribution distribution = new Distribution(counter.incrementAndGet(), amount, company.getId(), user.getId(), walletId, voucher.getBeginDate(), voucher.getEndDate());
        distributions.add(distribution);
        company.setBalance(balance - amount);
        return true;
    }

    private int getWalletId(Voucher voucher) {
        int walletId = -1;
        if (voucher instanceof GiftCard) {
            walletId = 1;
        }
        else if (voucher instanceof MealVoucher) {
            walletId = 2;
        }
        return walletId;
    }

    public List<Distribution> getDistributions() {
        return distributions;
    }
}

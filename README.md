# wedoogift-exercise


## Notes

- Quantités en Integer
- Problèmes de parsing de intput.json:
    - Pas d'historique des vouchers (begin/end date)
    - Lien entre wallet_type et wallet_id au niveau user
- TestExport (Level2) uniquement pour générer le même json que output-expected.json
- Utilisation d'un type parent "DataSet" pour faciliter l'export json
- Level 3 WIP..

## Ways of improvement
- Remplacer le Json parser par jackson 
- Json ordering
- Wallet type management
- MealVoucher endDate
- REST application to do..
- Supprimer "DataSet"
